const express = require('express');
const app = express();
// const publicPath = path.join(__dirname, '..', 'public');

// const port = process.env.PORT || 3000;

app.use(express.static("build"));

app.get("/", function (req, res) {
    res.send("<h1>Hello World!</h1>")
  })

  app.listen(process.env.PORT || 3000,
	() => console.log("Server is running..."));
