import React from 'react';
import firebase from '../../controller/Firebase';

import { Link } from 'react-router-dom';
import './Header.css';



const Header = () => {

  const handleClick = () => {
    const navLinks = document.querySelector(".nav-links");
    const links = document.querySelectorAll(".nav-links li");
    navLinks.classList.toggle("open");
    links.forEach(link => {
      link.classList.toggle("fade");
    });
  }


  return (
    <header>
      <nav className='nav-header'>
        <div className='hamburger' onClick={handleClick}>
          <div className='line'></div>
          <div className='line'></div>
          <div className='line'></div>
        </div>
        <ul className='nav-links'>
          <li><Link className='a' to='/home'>Home</Link></li>
          <li><Link className='a' to='/home'>Contact</Link></li>
          <li><button onClick={() => firebase.auth().signOut()}> Cerrar Seción</button></li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
