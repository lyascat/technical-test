import * as firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyA2bGp5igaJDqQRoQUWA22RxrJ3UFJDfrU',
  authDomain: 'technical-test-df264.firebaseapp.com',
  databaseURL: 'https://technical-test-df264.firebaseio.com',
  projectId: 'technical-test-df264',
  storageBucket: 'technical-test-df264.appspot.com',
  messagingSenderId: '549694632243',
  appId: '1:549694632243:web:a653a000794159dd598e46',
  measurementId: 'G-WMJ2B20W02'
};
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;
