import firebase from './Firebase';

const getProducts = (category) => {
  return firebase.firestore().collection('products').where('category', '==', category).get()
    .then((response) => {
      const arrayProduct = [];
      response.docs.forEach(doc => {
        const product = {
          id: doc.id,
          ...doc.data()
        };
        arrayProduct.push(product);
      });
      return arrayProduct;
    })
};


export default getProducts;
