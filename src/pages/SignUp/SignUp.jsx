import React, { useCallback } from "react";

import wave from '../../assets/img/fondo.svg';
import travel from '../../assets/img/travel.svg';
// import avatar from '../../assets/img/avatar.svg';


import { withRouter } from "react-router";
import firebase from "../../controller/Firebase";
import { Link } from 'react-router-dom';

const SignUp = ({ history }) => {
  const handleSignUp = useCallback(async event => {
    event.preventDefault();
    const { email, password } = event.target.elements;
    try {
      await firebase
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value);
      history.push("/");
    } catch (error) {
      alert(error);
    }
  }, [history]);

  const handleChangeUser = () =>{
    let input = document.querySelector('input[type="email"]')
    let element =input.parentElement.parentElement;
    let value = input.value;
    element.classList.add('focus');
    if(value === ''){
      element.classList.remove('focus');
    }
  }
  const handleChangePass = () =>{
    let input = document.querySelector('input[type="password"]')
    let element =input.parentElement.parentElement;
    let value = input.value;
    element.classList.add('focus');
    if(value === ''){
      element.classList.remove('focus');
    }
  }

  return (
    <div>
    <img className='wave' src={wave} alt='wave' />
    <div className='container-login'>
      <div className='img'>
        <img src={travel} alt='travel' />
      </div>
      <div className='login-content'>
        <form onSubmit={handleSignUp} action='index.html'>
          {/* <img src={avatar} alt='avatar' /> */}
          <h2 className='title'>Registrate</h2>
          <div className='input-div user'>
            <div className='i'>
              <i className='fas fa-user'></i>
            </div>
            <div className='div'>
              <h5>Username</h5>
              <input name="email" type='email' className='input' onChange = {handleChangeUser}/>
            </div>
          </div>
          <div className='input-div pass'>
            <div className='i'>
              <i className='fas fa-lock'></i>
            </div>
            <div className='div'>
              <h5>Password</h5>
              <input name='password' type='password' className='input' onChange={handleChangePass}/>
            </div>
          </div>
          <Link to='/'>Volver</Link>
          <input type='submit' className='btn' value='Login'/>
        </form>
      </div>
    </div>
  </div>
  );
};

export default withRouter(SignUp);
