import React from 'react';
import {HashRouter as Router, Route} from 'react-router-dom';

import { AuthProvider } from './Auth';

import Login from './pages/Login/Login';
import SignUp from './pages/SignUp/SignUp'
import Home from './pages/Home/Home';

import PrivateRoute from './PrivateRoute';

function App() {
  return (
    <AuthProvider>
      <Router>
        <div>
          <Route exact path="/" component={Login} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <PrivateRoute exact path="/home" component={Home} />
          {/* <PrivateRoute exact path="/programmed" component={Programmed} />
          <PrivateRoute exact path="/execution" component={Execution} />
          <PrivateRoute exact path="/finish" component={Finish} /> */}
        </div>
      </Router>
    </AuthProvider>
  );
}

export default App;
